package bigwell.helmes.persistence.dbo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity (name = "user_selection")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserSelectionDbo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "sectors_id")
    private long sectorsId;

}
