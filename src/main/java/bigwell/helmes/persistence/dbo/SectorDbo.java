package bigwell.helmes.persistence.dbo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table(name = "sectors")
@AllArgsConstructor
@NoArgsConstructor
public class SectorDbo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "value")
    private String value;

    @Column(name = "parent_id", nullable = true)
    private Long parentId;
}
