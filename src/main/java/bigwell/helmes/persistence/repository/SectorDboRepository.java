package bigwell.helmes.persistence.repository;

import bigwell.helmes.persistence.dbo.SectorDbo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SectorDboRepository extends JpaRepository<SectorDbo, Long> {
}
