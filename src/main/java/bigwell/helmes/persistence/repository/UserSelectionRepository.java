package bigwell.helmes.persistence.repository;

import bigwell.helmes.persistence.dbo.UserSelectionDbo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserSelectionRepository extends JpaRepository<UserSelectionDbo, Long> {
    List<UserSelectionDbo> findByUserId(Long userId);
    long deleteByUserId (Long userId);
}
