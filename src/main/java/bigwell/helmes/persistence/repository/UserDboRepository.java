package bigwell.helmes.persistence.repository;

import bigwell.helmes.persistence.dbo.UserDbo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserDboRepository extends JpaRepository<UserDbo, Long> {
    Optional<UserDbo> findByName(String name);
}
