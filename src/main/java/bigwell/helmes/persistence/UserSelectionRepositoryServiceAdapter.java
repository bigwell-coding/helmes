package bigwell.helmes.persistence;

import bigwell.helmes.persistence.dbo.UserSelectionDbo;
import bigwell.helmes.persistence.repository.UserSelectionRepository;
import bigwell.helmes.persistence.service.UserSelectionRepositoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class UserSelectionRepositoryServiceAdapter implements UserSelectionRepositoryService {

    private final UserSelectionRepository userDboRepository;

    @Override
    public void saveNewUserSelections(Long userId, List<Long> userSelections) {
        userDboRepository.deleteByUserId(userId);
        userSelections.forEach(sectorsId -> userDboRepository.save(UserSelectionDbo
                .builder()
                .userId(userId)
                .sectorsId(sectorsId)
                .build()));
    }

    @Override
    public List<UserSelectionDbo> getUserSectorSelections(Long userId) {
        return userDboRepository.findByUserId(userId);
    }
}
