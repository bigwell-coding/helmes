package bigwell.helmes.persistence.mapper;

import bigwell.helmes.core.model.Sector;
import bigwell.helmes.persistence.dbo.SectorDbo;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SectorDboMapper {
    Sector toModel (SectorDbo dto);
    List<Sector> toListModel (List<SectorDbo> model);
}
