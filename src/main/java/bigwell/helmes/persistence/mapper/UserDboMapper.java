package bigwell.helmes.persistence.mapper;

import bigwell.helmes.core.model.User;
import bigwell.helmes.persistence.dbo.UserDbo;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserDboMapper {
    User toModel (UserDbo dbo);
}
