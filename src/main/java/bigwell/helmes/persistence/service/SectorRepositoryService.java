package bigwell.helmes.persistence.service;

import bigwell.helmes.core.model.Sector;

import java.util.List;

public interface SectorRepositoryService {
    List<Sector> getAllSectorsForForm();
}
