package bigwell.helmes.persistence.service;

import bigwell.helmes.persistence.dbo.UserSelectionDbo;

import java.util.List;

public interface UserSelectionRepositoryService {
    void saveNewUserSelections(Long userId, List<Long> userSelections);
    List<UserSelectionDbo> getUserSectorSelections(Long userId);
}
