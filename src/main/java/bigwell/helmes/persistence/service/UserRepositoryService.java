package bigwell.helmes.persistence.service;


import bigwell.helmes.core.model.User;

public interface UserRepositoryService {
    Long saveNewUser(String name, Boolean termsAgreed);
    User findExistingUser(String name);
}
