package bigwell.helmes.persistence;

import bigwell.helmes.core.model.User;
import bigwell.helmes.persistence.dbo.UserDbo;
import bigwell.helmes.persistence.mapper.UserDboMapper;
import bigwell.helmes.persistence.repository.UserDboRepository;
import bigwell.helmes.persistence.service.UserRepositoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserRepositoryServiceAdapter implements UserRepositoryService {

    private final UserDboRepository userDboRepository;
    private final UserDboMapper dboMapper;

    @Override
    public Long saveNewUser(String name, Boolean termsAgreed) {
        User existingUser = findExistingUser(name);
        if(existingUser != null) {
            return null;
        }
        UserDbo dbo = userDboRepository.save(UserDbo.builder().name(name).termsAgreed(termsAgreed).build());
        return dbo.getId();
    }

    @Override
    public User findExistingUser(String name) {
        Optional<UserDbo> user = userDboRepository.findByName(name);
        return user.map(dboMapper::toModel).orElse(null);
    }
}
