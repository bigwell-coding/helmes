package bigwell.helmes.persistence;

import bigwell.helmes.core.model.Sector;
import bigwell.helmes.persistence.mapper.SectorDboMapper;
import bigwell.helmes.persistence.repository.SectorDboRepository;
import bigwell.helmes.persistence.service.SectorRepositoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class SectorRepositoryServiceAdapter implements SectorRepositoryService {

    private final SectorDboRepository sectorDboRepository;
    private final SectorDboMapper dboMapper;

    @Override
    public List<Sector> getAllSectorsForForm() {
        return dboMapper.toListModel(sectorDboRepository.findAll());
    }
}
