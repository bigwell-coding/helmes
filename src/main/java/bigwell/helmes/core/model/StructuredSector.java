package bigwell.helmes.core.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Optional;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StructuredSector {

    private Long id;
    private String value;
    private Long parentId;
    public static List<StructuredSector> originalList;

    public Integer findDepth() {
        return parentId == null ? 0 : findParent(parentId).findDepth() + 1;
    }

    private StructuredSector findParent(Long parentId) {
        Optional<StructuredSector> parent = originalList.stream()
                .filter(sector -> sector.getId().equals(parentId))
                .findFirst();
        return parent.orElse(null);
    }
}

