package bigwell.helmes.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StructuredSectorDto {

    private Long id;
    private String value;
    private Integer depth;

}
