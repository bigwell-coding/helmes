package bigwell.helmes.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginForm {
    private String name;
    private List<Long> selectedSectors;
    private Boolean termsAgree;
    private String message;
}
