package bigwell.helmes.rest;

import bigwell.helmes.rest.dto.LoginForm;
import bigwell.helmes.rest.dto.StructuredSectorDto;
import bigwell.helmes.rest.service.SectorService;
import bigwell.helmes.rest.service.UserFormService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class FormController {

    private final SectorService sectorService;

    private final UserFormService userFormService;

    @GetMapping("/sectors")
    List<StructuredSectorDto> getStructuredSectorsForForm() {
        return sectorService.getAllSectorsForForm();
    }

    @GetMapping("/get")
    LoginForm getFormData(@RequestParam String name) {
        return userFormService.getUserFormData(name);
    }

    @PostMapping("/create")
    ResponseEntity<String> saveNewSectorSelection(@RequestBody LoginForm loginForm) {
        String message = userFormService.saveUserFormData(loginForm);
        return !message.isEmpty() ? new ResponseEntity<>(message, HttpStatus.BAD_REQUEST) : new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/update")
    ResponseEntity<String> updateSectorSelection(@RequestBody LoginForm loginForm) {
        String message = userFormService.updateUserFormData(loginForm);
        return !message.isEmpty() ? new ResponseEntity<>(message, HttpStatus.BAD_REQUEST) : new ResponseEntity<>(HttpStatus.OK);
    }
}
