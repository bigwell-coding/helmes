package bigwell.helmes.rest.service;

import bigwell.helmes.rest.dto.StructuredSectorDto;

import java.util.List;

public interface SectorService {
    List<StructuredSectorDto> getAllSectorsForForm();
}
