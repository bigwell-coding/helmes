package bigwell.helmes.rest.service;

import bigwell.helmes.rest.dto.LoginForm;

import java.util.List;

public interface UserFormService {
    boolean isNotValidInput(LoginForm loginForm);
    String saveUserFormData(LoginForm loginForm);
    String updateUserFormData(LoginForm loginForm);
    LoginForm getUserFormData(String name);
}
