package bigwell.helmes.rest.adapter;

import bigwell.helmes.core.model.User;
import bigwell.helmes.persistence.dbo.UserSelectionDbo;
import bigwell.helmes.persistence.service.UserRepositoryService;
import bigwell.helmes.persistence.service.UserSelectionRepositoryService;
import bigwell.helmes.rest.dto.LoginForm;
import bigwell.helmes.rest.service.UserFormService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Transactional
public class UserFormServiceAdapter implements UserFormService {

    private final UserRepositoryService userRepositoryService;
    private final UserSelectionRepositoryService userSelectionRepositoryService;

    @Override
    public boolean isNotValidInput(LoginForm loginForm) {
        return !loginForm.getTermsAgree() || loginForm.getName().isBlank() || loginForm.getSelectedSectors().isEmpty();
    }

    @Override
    public String saveUserFormData(LoginForm loginForm) {
        if(isNotValidInput(loginForm)) {
            return "Form is not filled entirely!";
        }

        Long userId = userRepositoryService.saveNewUser(loginForm.getName(), loginForm.getTermsAgree());
        if(userId == null) {
            return "User with given name already exists!";
        }

        userSelectionRepositoryService.saveNewUserSelections(userId, loginForm.getSelectedSectors());
        return "";
    }

    @Override
    public String updateUserFormData(LoginForm loginForm) {
        if(isNotValidInput(loginForm)) {
            return "Form is not filled entirely!";
        }
        User user = userRepositoryService.findExistingUser(loginForm.getName());
        if(user == null) {
            return "No user with given name exists!";
        }

        userSelectionRepositoryService.saveNewUserSelections(user.getId(), loginForm.getSelectedSectors());
        return "";
    }

    @Override
    public LoginForm getUserFormData(String name) {
        User user = userRepositoryService.findExistingUser(name);
        if(user == null) {
            return LoginForm.builder().message("No user with given name found!").build();
        }
        List<UserSelectionDbo> selectionList = userSelectionRepositoryService.getUserSectorSelections(user.getId());
        List<Long> sectorIds = selectionList.stream().map(UserSelectionDbo::getSectorsId).collect(Collectors.toList());
        return LoginForm
                .builder()
                .name(user.getName())
                .termsAgree(user.getTermsAgreed())
                .selectedSectors(sectorIds)
                .build();

    }

}
