package bigwell.helmes.rest.adapter;

import bigwell.helmes.core.model.Sector;
import bigwell.helmes.core.model.StructuredSector;
import bigwell.helmes.rest.dto.StructuredSectorDto;
import bigwell.helmes.persistence.service.SectorRepositoryService;
import bigwell.helmes.rest.service.SectorService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class StructuredSectorServiceAdapter implements SectorService {

    private final SectorRepositoryService sectorRepositoryService;

    public List<StructuredSectorDto> getAllSectorsForForm() {
        List<Sector> sectorList = sectorRepositoryService.getAllSectorsForForm();
        List<StructuredSector> structuredSector = toStructuredSectors(sectorList);
        return toStructuredSectorDtos(structuredSector);
    }

    private List<StructuredSector> toStructuredSectors(List<Sector> list) {
        List<StructuredSector> structuredSectors = new ArrayList<>();
        list.forEach(sector -> structuredSectors.add(
                StructuredSector
                .builder()
                .id(sector.getId())
                .value(sector.getValue())
                .parentId(sector.getParentId())
                .build()));
        StructuredSector.originalList = new ArrayList<>(structuredSectors);

        return orderStructuredSectors(new ArrayList<>(), structuredSectors, null);
    }

    private List<StructuredSectorDto> toStructuredSectorDtos(List<StructuredSector> list) {
        List<StructuredSectorDto> structuredSectors = new ArrayList<>();
        list.forEach(sector -> structuredSectors.add(
                StructuredSectorDto
                        .builder()
                        .id(sector.getId())
                        .value(sector.getValue())
                        .depth(sector.findDepth())
                        .build()));
        return structuredSectors;
    }

    private List<StructuredSector> orderStructuredSectors(
            List<StructuredSector> orderedList,
            List<StructuredSector> unOrderedList,
            StructuredSector parent) {
        if(parent == null) {
            List<StructuredSector> noParentList = unOrderedList.stream().filter(sector -> sector.getParentId() == null).collect(Collectors.toList());
            unOrderedList.removeAll(noParentList);

            noParentList.forEach(sector -> {
                orderedList.add(sector);
                orderStructuredSectors(orderedList, unOrderedList, sector);
            });

        } else {
            if(unOrderedList.stream().anyMatch(sector -> sector.getParentId().equals(parent.getId()))) {
                List<StructuredSector> childrenList = unOrderedList
                        .stream()
                        .filter(sector -> sector.getParentId().equals(parent.getId()))
                        .collect(Collectors.toList());
                childrenList.forEach(sector -> {
                    orderedList.add(sector);
                    unOrderedList.remove(sector);
                    orderStructuredSectors(orderedList, unOrderedList, sector);
                });
            }
        }
        return orderedList;
    }

}
