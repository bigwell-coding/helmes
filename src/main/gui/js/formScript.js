$(document).ready(function(){

    $.ajax({ url: "http://localhost:8080/sectors",
        type: "GET",
        success: function(jsonData){
            $('#sector-select').empty();
            $.each(jsonData, function(id, sector) {
                $('#sector-select').append($('<option>', {
                          value: sector.id,
                          text: sector.value
                      })
                      .addClass("depth-" + sector.depth)
                );
            });
        }
    });

    $("#submit-create").attr("disabled", "disabled");
    $("#submit-update").attr("disabled", "disabled");
    hideAlertMessageAndRemoveStatusClasses();

    actionListeners()

});

function actionListeners() {

    createRequest();
    updateRequest();
    getRequest();

    $('#termsAgree').click(function() {
        updateButtonBasedOnCheckbox();
    });
}

function getRequest() {
    $("#submit-get").click(function(e) {
        hideAlertMessageAndRemoveStatusClasses();

        var name = $("#inputName").val();
        var urlParam = "name="+name;

        $.ajax({
           type: "GET",
           url: "http://localhost:8080/get",
           data: urlParam,
           success: function(data) {
                if(data.message == null || data.message == '') {
                   $("#sector-select").val(data.selectedSectors).change();
                   $('#termsAgree').prop('checked', data.termsAgree);
                   updateButtonBasedOnCheckbox();
                   showSuccessAlert("Data Retrieved!");
                } else {
                    showErrorAlert(data.message);
                }
           }
        });
    });
}

function updateRequest() {
    $("#submit-update").click(function(e) {
        hideAlertMessageAndRemoveStatusClasses();

        var selectedSectors = $("#sector-select :selected").map((_, e) => e.value).get();
        var name = $("#inputName").val();
        var terms = $('#termsAgree').is(":checked");
        var json = {"name" : name, "selectedSectors" : selectedSectors, "termsAgree": terms.toString()}

        $.ajax({
           type: "POST",
           url: "http://localhost:8080/update",
           data: JSON.stringify(json),
           contentType:"application/json",
           success: function(data) {
               showSuccessAlert("Data Updated!");
           },
           error: function(xhr, ajaxOptions, thrownError) {
               showErrorAlert(xhr.responseText);
           }
        });
    });
}

function createRequest() {
    $("#submit-create").click(function(e) {
        hideAlertMessageAndRemoveStatusClasses();

        var selectedSectors = $("#sector-select :selected").map((_, e) => e.value).get();
        var name = $("#inputName").val();
        var terms = $('#termsAgree').is(":checked");
        var json = {"name" : name, "selectedSectors" : selectedSectors, "termsAgree": terms.toString()}

        $.ajax({
           type: "POST",
           url: "http://localhost:8080/create",
           data: JSON.stringify(json),
           contentType:"application/json",
           success: function(data) {
               showSuccessAlert("Data saved!")
           },
           error: function(xhr, ajaxOptions, thrownError) {
               showErrorAlert(xhr.responseText);
           }
        });
    });
}

function updateButtonBasedOnCheckbox() {
    if ($('#termsAgree').is(":checked")) {
        $("#submit-create").removeAttr("disabled");
        $("#submit-update").removeAttr("disabled");
    } else {
        $("#submit-create").attr("disabled", "disabled");
        $("#submit-update").attr("disabled", "disabled");
    }
}

function hideAlertMessageAndRemoveStatusClasses() {
    $("#requestMessage").hide();
    $("#requestMessage").removeClass("alert-success");
    $("#requestMessage").removeClass("alert-danger");
}

function showSuccessAlert (text) {
    $("#requestMessage").show();
    $("#requestMessage").text(text);
    $("#requestMessage").addClass("alert-success");
    $("#requestMessage").delay(3000).fadeOut('slow');
}

function showErrorAlert (text) {
    $("#requestMessage").show();
    $("#requestMessage").text(text);
    $("#requestMessage").addClass("alert-danger");
    $("#requestMessage").delay(3000).fadeOut('slow');
}


