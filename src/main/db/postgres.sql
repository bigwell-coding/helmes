--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1
-- Dumped by pg_dump version 14.1

-- Started on 2022-01-05 14:15:06

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3331 (class 0 OID 0)
-- Dependencies: 4
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 213 (class 1259 OID 16420)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 211 (class 1259 OID 16397)
-- Name: sectors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sectors (
    id bigint NOT NULL,
    value character varying NOT NULL,
    parent_id bigint
);


ALTER TABLE public.sectors OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 16400)
-- Name: user_selection; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_selection (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    sectors_id bigint NOT NULL
);


ALTER TABLE public.user_selection OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16394)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying NOT NULL,
    terms_agreed boolean NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 3323 (class 0 OID 16397)
-- Dependencies: 211
-- Data for Name: sectors; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sectors (id, value, parent_id) FROM stdin;
1	Manufacturing	\N
2	Service	\N
3	Other	\N
37	Creative industries	3
29	Energy technology	3
33	Environment	3
25	Business services	2
35	Engineering	2
28	Information Technology and Telecommunications	2
22	Tourism	2
141	Translation services	2
21	Transport and Logistics	2
581	Data processing, Web portals, E-marketing	28
576	Programming, Consultancy	28
121	Software, Hardware	28
122	Telecommunications	28
111	Air	21
114	Rail	21
112	Road	21
113	Water	21
19	Construction materials	1
18	Electronics and Optics	1
6	Food and Beverage	1
13	Furniture	1
12	Machinery	1
11	Metalworking	1
9	Plastic and Rubber	1
5	Printing	1
7	Textile and Clothing	1
8	Wood	1
43	Beverages	6
437	Other	6
389	Bathroom/sauna	13
385	Bedroom	13
390	Children’s room	13
98	Kitchen	13
101	Living room	13
392	Office	13
394	Other (Furniture)	13
341	Outdoor	13
99	Project furniture	13
94	Machinery components	12
91	Machinery equipment/tools	12
224	Manufacture of machinery	12
97	Maritime	12
93	Metal structures	12
508	Other	12
227	Repair and maintenance service	12
271	Aluminium and steel workboats	97
269	Boat/Yacht building	97
230	Ship repair and conversion	97
67	Construction of metal structures	11
263	Houses and buildings	11
267	Metal products	11
542	Metal works	11
75	CNC-machining	542
62	Forgings, Fasteners	542
69	Gas, Plasma, Laser cutting	542
66	MIG, TIG, Aluminum welding	542
54	Packaging	9
556	Plastic goods	9
559	Plastic processing technology	9
560	Plastic profiles	9
55	Blowing	559
57	Moulding	559
53	Plastics welding and processing	559
148	Advertising	5
150	Book/Periodicals printing	5
145	Labelling and packaging printing	5
44	Clothing	7
45	Textile	7
337	Other (Wood)	8
51	Wooden building materials	8
47	Wooden houses	8
42	Fish & fish products	6
40	Meat & meat products	6
39	Milk & dairy products	6
378	Sweets & snack food	6
342	Bakery & confectionery products	6
\.


--
-- TOC entry 3324 (class 0 OID 16400)
-- Dependencies: 212
-- Data for Name: user_selection; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_selection (id, user_id, sectors_id) FROM stdin;
25	17	111
26	17	114
27	17	112
28	17	113
34	13	19
35	13	18
36	13	6
48	45	576
71	55	389
72	55	385
73	55	390
75	74	25
76	74	35
77	74	22
79	78	54
80	78	556
81	78	560
82	3	29
83	12	19
84	12	43
\.


--
-- TOC entry 3322 (class 0 OID 16394)
-- Dependencies: 210
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, terms_agreed) FROM stdin;
3	Martin	t
5	Maria	t
9	Jaanus	t
10	Mihkel	t
12	Olivia	t
13	Sandra	t
17	Sander	t
45	Mari	t
55	Ave	t
74	Jenny	t
78	Mare	t
\.


--
-- TOC entry 3332 (class 0 OID 0)
-- Dependencies: 213
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hibernate_sequence', 84, true);


--
-- TOC entry 3178 (class 2606 OID 16410)
-- Name: sectors sectors_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sectors
    ADD CONSTRAINT sectors_pk PRIMARY KEY (id);


--
-- TOC entry 3174 (class 2606 OID 16408)
-- Name: users user_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT user_pk PRIMARY KEY (id);


--
-- TOC entry 3180 (class 2606 OID 16414)
-- Name: user_selection user_selection_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_selection
    ADD CONSTRAINT user_selection_pk PRIMARY KEY (id);


--
-- TOC entry 3176 (class 2606 OID 16422)
-- Name: users users_name_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_name_un UNIQUE (name);


--
-- TOC entry 3182 (class 2606 OID 16423)
-- Name: user_selection user_selection_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_selection
    ADD CONSTRAINT user_selection_fk FOREIGN KEY (id) REFERENCES public.user_selection(id) ON UPDATE CASCADE;


--
-- TOC entry 3181 (class 2606 OID 16415)
-- Name: user_selection users_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_selection
    ADD CONSTRAINT users_fk FOREIGN KEY (user_id) REFERENCES public.users(id);


-- Completed on 2022-01-05 14:15:06

--
-- PostgreSQL database dump complete
--

